const Sequelize = require('sequelize');
const dbConfig = require('../config/database');
const connection = new Sequelize(dbConfig);

//REQUIRE MODELS
const Permission = require('../models/Permission');
const User = require('../models/User');

//INIT CONNECTIONS
Permission.init(connection)
User.init(connection)


//id de permissao vai para usuario
Permission.hasMany(User);
User.belongsTo(Permission);

//sync modeukes
Permission.sync()
User.sync()

module.exports = connection;