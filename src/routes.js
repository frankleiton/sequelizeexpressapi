const express = require('express');
const routes = express.Router();
const jwt = require('jsonwebtoken');

const UserController = require('./controllers/UserController');

routes.get('/users', verifyToken, UserController.index);
routes.post('/users', UserController.store);
routes.put('/users/:id', UserController.updateUser);
routes.get('/users/:id', UserController.getUserById);
routes.post('/users/login', UserController.login);

function verifyToken(req, res, next) {
    const bearerHeader = req.headers['authorization'];
    
    if (typeof bearerHeader !== 'undefined') {

        jwt.verify(req.headers['authorization'].split(' ')[1], 'key', function(err, decoded) {
            if (err) {
                res.sendStatus(403);
            }else{
                next();
            }
          });
    }else{
        //Forbidden
        res.sendStatus(403);
    }
}

module.exports = routes;